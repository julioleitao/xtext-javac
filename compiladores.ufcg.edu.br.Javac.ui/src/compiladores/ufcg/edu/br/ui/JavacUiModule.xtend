/*
 * generated by Xtext 2.10.0
 */
package compiladores.ufcg.edu.br.ui

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class JavacUiModule extends AbstractJavacUiModule {
}
